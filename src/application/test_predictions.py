from os import path
import sys
import pandas as pd

import pickle

from sklearn.metrics import accuracy_score, precision_score, recall_score, fbeta_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

import src.settings.settings as stg
import src.settings.user_settings as ustg
from src.domain.train_model import train_model

#Import X_test and y_pred
try:
    with open(stg.MODEL_FILENAME, 'rb') as handle:
        data_model_dict = pickle.load(handle)
    model = data_model_dict['model']
    X_test = data_model_dict['X_test']
    y_pred = data_model_dict['y_pred']
except FileNotFoundError:
    print("No model was found. Please train a model first.")
    data_model_dict = train_model()
    model = data_model_dict['model']

#Import y_test labels and map to {0,1}
y_test = pd.read_csv(path.abspath(path.join(stg.TEST_DATA_DIR, ustg.LABELS_TEST_FILENAME)))
y_test = y_test['CHURN'].map({'No': 0, 'Yes': 1})

#Results
print("======PREDICTION======")
print("model fbeta: %.3f" % fbeta_score(y_test, y_pred, beta=2))
print("model recall: %.3f" % recall_score(y_test, y_pred))
print("model accuracy: %.3f" % model.score(X_test, y_test))
print("model precision: %.3f" % precision_score(y_test, y_pred))

print("Confusion Matrix")
print(confusion_matrix(y_test, y_pred))

print("Classification Report")
print(classification_report(y_test, y_pred))
