.. _technicalcleaningmodule: technicalcleaning

Module technical_cleaning
*************************
This document describes all classes of the module technical_cleaning.

.. automodule:: src.infrastructure.technical_cleaning

============================================================
Class
============================================================

.. autoclass:: src.infrastructure.technical_cleaning.DateTransformer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.infrastructure.technical_cleaning.DateTransformer.transform()

============================================================
Class
============================================================

.. autoclass:: src.infrastructure.technical_cleaning.CategoricalTypeTransformer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.infrastructure.technical_cleaning.CategoricalTypeTransformer.transform()

============================================================
Class
============================================================

.. autoclass:: src.infrastructure.technical_cleaning.BooleanEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: src.infrastructure.technical_cleaning.BooleanEncoder.transform()







    