.. _rawbankdatamodule: rawbankdata

Module raw_bank_data
********************
This document describes all classes of the module raw_bank_data.

.. automodule:: src.infrastructure.raw_bank_data

============================================================
    Class
============================================================

.. autoclass:: src.infrastructure.raw_bank_data.RawBankData()

************************************************************
    Properties
************************************************************

    .. autoproperty:: src.infrastructure.raw_bank_data.RawBankData.data()
    .. autoproperty:: src.infrastructure.raw_bank_data.RawBankData.features()
    .. autoproperty:: src.infrastructure.raw_bank_data.RawBankData.target()
