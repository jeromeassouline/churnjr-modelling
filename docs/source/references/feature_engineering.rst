.. _featureengineeringmodule: featureengineering

Module feature_engineering
**************************
This document describes all classes of the module feature_engineering.

.. automodule:: src.domain.feature_engineering

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.SeniorityCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.SeniorityCreator.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.AgeClassCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.AgeClassCreator.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.CreditScoreAgeRatioCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.CreditScoreAgeRatioCreator.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.BalanceWageRatioCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.BalanceWageRatioCreator.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.CreditScoreByNProductsCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.CreditScoreByNProductsCreator.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.feature_engineering.FeatureDropper()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.feature_engineering.FeatureDropper.transform()

